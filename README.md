# vant-number-plate

## vue-cli
本项目基于@vue/cli 4.5.13创建

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## 说明
这是一个vue移动端车牌号输入组件，此组件依赖vant的van-action-sheet和van-button等组件，同时使用了less

## 效果图
![](./doc/img/vant-number-plate-1.jpg)

![](./doc/img/vant-number-plate-2.jpg)

## 演示地址
- 手机扫码体验  
  ![](./doc/img/vnp-qrcode.png)

- [点击查看演示](http://www.lishumin.top:3001/vant-number-plate/)

## 使用示例
- 示例一：只使用键盘，自定义显示组件
  ```html
  <template>
    <div>
      <h3>示例一</h3>
      <van-cell-group>
        <van-field :value="value" readonly label="车牌号" @click="show = true" />
      </van-cell-group>
      <vnp-keyboard v-model="value" :show.sync="show"></vnp-keyboard>
    </div>
  </template>

  <script>
  import Keyboard from '@/components/vant-number-plate/vnp-keyboard';

  export default {
    components: {
      'vnp-keyboard': Keyboard,
    },
    data() {
      return {
        show: false,
        value: '川A'
      }
    }
  }
  </script>

  <style lang="less" scoped>
    h3 {
      padding: 0 30px;
    }
  </style>
  ```

- 示例二：使用提供的显示组件
  ```html
  <template>
    <div>
      <h3>示例二</h3>
      <div class="demo-two-box">
        <vnp-input v-model="value"></vnp-input>
      </div>
    </div>
  </template>

  <script>
  import VnpInput from '@/components/vant-number-plate/vnp-input';

  export default {
    components: {
      'vnp-input': VnpInput
    },
    data() {
      return {
        value: '川A'
      }
    }
  }
  </script>

  <style lang="less" scoped>
    .demo-two-box {
      padding: 0 30px;
    }
    h3 {
      padding: 0 30px;
    }
  </style>
  ```
